#!/usr/bin/python3

"""Test cases for softwareproperties.AptAuth class."""

import os
import subprocess
import time
import unittest
import unittest.mock

import softwareproperties.AptAuth

KINETIC_DEFAULT = """\
tru:t:1:1658826897:0:3:1:5
pub:-:4096:1:D94AA3F0EFE21092:1336774248:::-:::scSC::::::23::0:
fpr:::::::::843938DF228D22F7B3742BC0D94AA3F0EFE21092:
uid:-::::1336774248::77355A0B96082B2694009775B6490C605BD16B6F::\
Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>::::::::::0:
pub:-:4096:1:871920D1991BC93C:1537196506:::-:::scSC::::::23::0:
fpr:::::::::F6ECB3762474EDA9D21B7022871920D1991BC93C:
uid:-::::1537196506::BE438F08F546424C0EA810FD722053597EB5127B::\
Ubuntu Archive Automatic Signing Key (2018) <ftpmaster@ubuntu.com>::::::::::0:
"""


class TestAptAuth(unittest.TestCase):
    """Test softwareproperties.AptAuth class."""

    @staticmethod
    def setUp():
        os.environ["TZ"] = "UTC"
        time.tzset()

    @unittest.mock.patch("subprocess.run")
    def test_list(self, run_mock):
        """Test softwareproperties.AptAuth.list."""
        apt_auth = softwareproperties.AptAuth.AptAuth()
        cmd = [
            "/usr/bin/apt-key",
            "--quiet",
            "adv",
            "--with-colons",
            "--batch",
            "--fixed-list-mode",
            "--list-keys",
        ]
        run_mock.return_value = subprocess.CompletedProcess(
            args=cmd,
            returncode=0,
            stdout=KINETIC_DEFAULT,
            stderr="Warning: apt-key is deprecated.\n",
        )
        self.assertEqual(
            apt_auth.list(),
            [
                "D94AA3F0EFE21092 2012-05-11\n"
                "Ubuntu CD Image Automatic Signing Key (2012)"
                " <cdimage@ubuntu.com>",
                "871920D1991BC93C 2018-09-17\n"
                "Ubuntu Archive Automatic Signing Key (2018)"
                " <ftpmaster@ubuntu.com>",
            ],
        )
        run_mock.assert_called_with(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
        )


if __name__ == "__main__":
    unittest.main()
